﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
    public static class Program
    {
        private static HubConnection _connection;

        public static async Task Main(string[] args)
        {
            _connection = new HubConnectionBuilder()
                .WithUrl("http://localhost:5000/streamHub")
                .ConfigureLogging(logging =>
                {
                    logging.AddDebug();
                    logging.SetMinimumLevel(LogLevel.Debug);
                })
                .Build();

            await _connection.StartAsync();

            var cancellationTokenSource = new CancellationTokenSource();

            var stream = _connection.StreamAsync<string>("SendDataRow", cancellationTokenSource.Token);

            var timer = new Stopwatch();
            timer.Start();

            await foreach (var data in stream.WithCancellation(cancellationTokenSource.Token)) Console.WriteLine($"{data}");

            timer.Stop();

            Console.WriteLine($"Streaming completed in {timer.Elapsed:m\\:ss\\.fff} ");

            Console.ReadLine();
        }
    }
}
