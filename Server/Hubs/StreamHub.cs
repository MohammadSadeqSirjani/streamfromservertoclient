﻿using Microsoft.Extensions.Logging;
using Npgsql;
using Server.Controllers;
using System.Collections.Generic;

namespace Server.Hub
{
    public class StreamHub : Microsoft.AspNetCore.SignalR.Hub
    {
        private readonly ILogger<HomeController> _logger;

        public StreamHub(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async IAsyncEnumerable<string> SendDataRow()
        {
            const string connectionString = "host=localhost;username=postgres;password=Sa@12345678;database=dvdrental";

            const string query = "SELECT * FROM rental";

            await using var connection = new NpgsqlConnection(connectionString);

            var command = new NpgsqlCommand(query, connection);

            connection.Open();

            var reader = await command.ExecuteReaderAsync();

            while (await reader.ReadAsync())
            {
                var response = $"rental_id: {reader[0]}, rental_date: {reader[1]}, inventory_id: {reader[2]}, customer_id: {reader[3]}, return_date: {reader[4]}, staff_id: {reader[5]}, last_update: {reader[6]}"
                    .ToString();
                _logger.LogInformation(response);
                yield return response;
            }

            await reader.CloseAsync();
        }
    }
}
